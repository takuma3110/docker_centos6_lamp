#!/bin/bash
#もろもろルール化&設定ファイル化したい
#Ansible化したい
#複数のCoreOSをまたいでectdとかfleetとかでデプロイしたい
#Jenkins先生も使いたい
#dbのデータ保持dump->git以外になにか

source ./env.bash_profile
WORK_DIR=`pwd`
build(){
  echo 'build(create image)'
	#storage
	#@see http://qiita.com/mopemope/items/b05ff7f603a5ad74bf55
	docker build -t "${DOCKER_ACCOUNT_NAME}/${APP_NAME}_storage" server/docker/storage
	
	#base
#	if [ ! -e ./server/keys/id_rsa.pub -o ! -e ./server/keys/id_rsa ]; then
	if [ ! -e ./server/docker/base/id_rsa.pub ]; then
		./server/bin/gen_keys.sh ./server/keys/
		cp ./server/keys/id_rsa.pub ./server/docker/base/
	fi
	
	docker build -t "${DOCKER_ACCOUNT_NAME}/${APP_NAME}_base" server/docker/base
	#docker run -d --name "${APP_NAME}_base" "${DOCKER_ACCOUNT_NAME}/${APP_NAME}_base"
	#rm -f ./server/docker/base/id_rsa.pub
	
	#db
	cd server/docker/db/
	mv Dockerfile Dockerfile.org
	cat "Dockerfile.org" | sed \
	  -e "s/@@@YOUR_DOCKER_ACCOUNT_NAME@@@/$DOCKER_ACCOUNT_NAME/g" \
	  -e "s/@@@YOUR_APP_NAME@@@/$APP_NAME/g" \
	  > Dockerfile
	docker build -t "${DOCKER_ACCOUNT_NAME}/${APP_NAME}_db" .
	rm -f Dockerfile
	mv Dockerfile.org Dockerfile
	cd $WORK_DIR;

	#web
	cd server/docker/web/
	mv Dockerfile Dockerfile.org
	cat "Dockerfile.org" | sed \
	  -e "s/@@@YOUR_DOCKER_ACCOUNT_NAME@@@/$DOCKER_ACCOUNT_NAME/g" \
	  -e "s/@@@YOUR_APP_NAME@@@/$APP_NAME/g" \
	  > Dockerfile
	docker build -t "${DOCKER_ACCOUNT_NAME}/${APP_NAME}_web" .
	rm -f Dockerfile
	mv Dockerfile.org Dockerfile
	cd $WORK_DIR;
}

run(){
  echo 'run'
	#storage
	docker run -d -t -i --name "${APP_NAME}_storage" "${DOCKER_ACCOUNT_NAME}/${APP_NAME}_storage"
#docker start "${APP_NAME}_storage"; docker attach "${APP_NAME}_storage"
#Ctrl+P Ctrl+Q でdetach

#ホスト側にコピー？
#docker cp
#docker export
	
	#db
	docker run -d -h "${APP_NAME}_db" --name "${APP_NAME}_db" -p 3306:3306 -v $(pwd)/db:/usr/local/db --volumes-from "${APP_NAME}_storage" "${DOCKER_ACCOUNT_NAME}/${APP_NAME}_db"	
	
	#web
	docker run -d -h "${APP_NAME}_web" --name "${APP_NAME}_web" -p 80:80 -v $(pwd)/app:/var/www/app --link "${APP_NAME}_db":db  "${DOCKER_ACCOUNT_NAME}/${APP_NAME}_web"
}
orchestrate(){
  echo 'orchestrate'
	./server/bin/ssh_container.sh "${APP_NAME}_db" "echo 'grant all privileges on '*'.'*' to root@"'"%"'";'|mysql -uroot"
	./server/bin/ssh_container.sh "${APP_NAME}_db" "cat /usr/local/db/DDL.sql |mysql -uroot"
	
	#init laravel app
	#./ssh_container.sh "${APP_NAME}_web" "laravel new web"
	#./ssh_container.sh "${APP_NAME}_web" "mv web /var/www/app/"
	#./ssh_container.sh "${APP_NAME}_web" "chmod -R 777 /var/www/app/web/app/storage"
}
configure(){
  echo 'configure(run image as container and orchestrate)'
  run
  sleep 20
  orchestrate
}
dump(){
  echo 'dump(save storage to db/DUMP.sql)'
	./server/bin/ssh_container.sh "${APP_NAME}_db" "mysqldump -u root -x --all-databases > /usr/local/db/DUMP.sql"
}
restore(){
  echo 'restore(read storage from db/DUMP.sql)'
	./server/bin/ssh_container.sh "${APP_NAME}_db" "if [ -e /usr/local/db/DUMP.sql ]; then (mysql -u root < /usr/local/db/DUMP.sql) fi"
}
destroy-with-storage(){
  echo 'destroy-with-storage(stop and delete container)'
  dump
	docker rm -f "${APP_NAME}_storage"
	docker rm -f "${APP_NAME}_web"
	docker rm -f "${APP_NAME}_db"
}
destroy(){
  echo 'destroy(stop and delete container)'
	docker rm -f "${APP_NAME}_web"
	docker rm -f "${APP_NAME}_db"
}
clean(){
  echo 'clean(delete image)'
    rm -f ./server/keys/id*
    rm -f ./server/docker/base/id_rsa.pub
	destroy-with-storage
	
	docker rmi "${DOCKER_ACCOUNT_NAME}/${APP_NAME}_web"
	docker rmi "${DOCKER_ACCOUNT_NAME}/${APP_NAME}_db"
	docker rmi "${DOCKER_ACCOUNT_NAME}/${APP_NAME}_storage"
	docker rmi "${DOCKER_ACCOUNT_NAME}/${APP_NAME}_base"

	#停止しているコンテナを削除
	#docker rm `docker ps -a -q`
	
	#野良イメージを削除
	#docker rmi $(docker images | awk '/^<none>/ { print $3 }')

}

init(){
  echo 'init(all clean(conatins container,image,keys) and start(build, configure))'
	clean
	build
	configure
}

start() {
  echo 'start'
	docker start "${APP_NAME}_storage"
	docker start "${APP_NAME}_db"
	docker start "${APP_NAME}_web"
  
}
stop() {
  echo 'stop'
	docker stop "${APP_NAME}_web"
	docker stop "${APP_NAME}_db"
	docker stop "${APP_NAME}_storage"
}
status() {
  echo 'status'
}

# See how we were called.
case "$1" in
  build)
        build
        ;;
  dump)
  		dump
        ;;
  restore)
  		restore
        ;;
  clean)
        clean
        ;;
  run)
  		run
        ;;
  re-build-run)
  		destroy
  		build
  		run
        ;;
  orchestrate)
  		orchestrate
        ;;
  configure)
  		configure
        ;;
  destroy)
        destroy
        ;;
  re-configure)
		destroy
  		configure
        ;;
  init)
        init
        ;;
  start)
        start
        ;;
  stop)
        stop
        ;;
  restart)
        stop
        start
        ;;
#  status)
#  		status
#        ;;
  *)
        echo $"Usage: $prog {init|build|clean|configure|destroy|re-configure|start|stop|restart}"
        RETVAL=2
esac

exit 1

#最初にinit
#全て削除はclean

#一度起動したら
# appのビルドしなおしはre-build-run
# 構成しなおしはre-configure
#一時的な停止はstop,start,restart
# storageは削除されない


