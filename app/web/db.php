<?php
//$__prepares = false;
$__prepares = array();

function _prepare($db, $sql){
	global $__prepares;
	if($__prepares === false)return $db->prepare($sql);
	$key = md5($sql);
	if(!isset($prepares[$key])){
		$prepares[$key] = $db->prepare($sql);
	}
	return $prepares[$key];
}
function query($db, $sql, $param = array()){
	$stm = _prepare($db, $sql);
	if(!$stm){
		$info = $db->errorInfo();
		echo 'pdo error ' . $info[1] . ':' . $info[2] . "\n";
		exit;
	}	
	$stm->execute($param);
	return $stm->fetchAll(PDO::FETCH_ASSOC);
}
function update_query($db, $sql, $param = array()){
	$stm = _prepare($db, $sql);
	$res = $stm->execute($param);
	if(!$res){
		$info = $stm->errorInfo();
		echo 'pdo error ' . $info[1] . ':' . $info[2] . "\n";
		exit;
	}	
}

function db_count($db, $table_name, $where = array()){
	$sql = 'select count(*) as c from ' . $table_name . ' ';
	$param = array();
	if(count($where)>0){
		$sql .= ' where ';
		$key_and_placeholders = array();
		foreach ($where as $key => $value) {
			$key_and_placeholders[] = $key . '=?';
			$param[] = $value;
		}
		$sql .= implode(' and ', $key_and_placeholders);
	}
	$sql .= ';';
	$res = query($db, $sql, $param);
	return $res[0]['c'];
}
function select($db, $table_name, $where = array(), $additional_sql = ''){
	$sql = 'select * from ' . $table_name . ' ';
	$param = array();
	if(count($where)>0){
		$sql .= ' where ';
		$key_and_placeholders = array();
		foreach ($where as $key => $value) {
			$key_and_placeholders[] = $key . '=?';
			$param[] = $value;
		}
		$sql .= implode(' and ', $key_and_placeholders);
	}
	$sql .= ' ' . $additional_sql;
	$sql .= ';';
	return query($db, $sql, $param);
}
function insert($db, $table_name, $named_params){
	$sql = 'insert into ' . $table_name . ' (';
	$keys = array();
	$placeholders = array();
	$param = array();
	foreach ($named_params as $key => $value) {
		$keys[] = $key;
		$placeholders[] = '?';
		$param[] = $value;
	}
	$sql .= implode(',', $keys);
	$sql .= ') values(';
	$sql .= implode(',', $placeholders);
	$sql .= ');';
//echo $sql . "\n";
//var_dump($param);
	$stm = _prepare($db, $sql);
	$res = $stm->execute($param);
	if(!$res){
		$info = $stm->errorInfo();
		echo 'pdo error ' . $info[1] . ':' . $info[2] . "\n";
		echo $sql . "\n";
		var_dump($param);
		exit;
	}else{
		if(DEBUG)echo "insert $table_name"  ."\n";
		if(defined('DISPLAY_SQL') && DISPLAY_SQL){
			echo $sql . "\n";
			var_dump($param);
		}
	}
	return $res;
}
function update($db, $table_name, $named_params, $where){
	$sql = 'update ' . $table_name . ' set ';
	$key_and_placeholders = array();
	$param = array();
	foreach ($named_params as $key => $value) {
		$key_and_placeholders[] = $key . '=?';
		$param[] = $value;
	}
	$sql .= implode(',', $key_and_placeholders);
	$sql .= ' where ';
	
	$key_and_placeholders = array();
	$key_and_values = array();
	foreach ($where as $key => $value) {
		$key_and_placeholders[] = $key . '=?';
		$key_and_values[] = $key . '=' . $value;
		$param[] = $value;
	}
	$sql .= implode(' and ', $key_and_placeholders);
	$sql .= ';';
	$stm = _prepare($db, $sql);
	$res = $stm->execute($param);
	if(!$res){
		$info = $stm->errorInfo();
		echo 'pdo error ' . $info[1] . ':' . $info[2] . "\n";
		echo $sql . "\n";
		var_dump($param);
		exit;
	}else{
		if(DEBUG)echo "update $table_name " . implode(' and ' , $key_and_values) ."\n";
		if(defined('DISPLAY_SQL') && DISPLAY_SQL){
			echo $sql . "\n";
			var_dump($param);
		}
	}
	return $res;
}
function delete($db, $table_name, $where= array()){
	$sql = 'delete from ' . $table_name .' ';
	$key_and_values = array();
	$key_and_placeholders = array();
	$param = array();
	if($where){
		$sql .= ' where ';
		foreach ($where as $key => $value) {
			$key_and_placeholders[] = $key . '=?';
			$key_and_values[] = $key . '=' . $value;
			$param[] = $value;
		}
		$sql .= implode(' and ', $key_and_placeholders);
		$sql .= ';';
	}
	$stm = _prepare($db, $sql);
	$res = $stm->execute($param);
	if(!$res){
		$info = $stm->errorInfo();
		echo 'pdo error ' . $info[1] . ':' . $info[2] . "\n";
		echo $sql . "\n";
		var_dump($param);
		exit;
	}else{
		if(DEBUG)echo "delete $table_name " . implode(' and ' , $key_and_values) ."\n";
		if(defined('DISPLAY_SQL') && DISPLAY_SQL){
			echo $sql . "\n";
			var_dump($param);
		}
	}
	return $res;
}