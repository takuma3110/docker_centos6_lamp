#!/bin/sh
#/usr/bin/mysqld_safe

#
# mysqld        This shell script takes care of starting and stopping
#               the MySQL subsystem (mysqld).
#
# chkconfig: - 64 36
# description:  MySQL database server.
# processname: mysqld
# config: /etc/my.cnf
# pidfile: /var/run/mysqld/mysqld.pid

# Source function library.
. /etc/rc.d/init.d/functions

# Source networking configuration.
. /etc/sysconfig/network


exec="/usr/bin/mysqld_safe"
prog="mysqld"

# Set timeouts here so they can be overridden from /etc/sysconfig/mysqld
STARTTIMEOUT=120
STOPTIMEOUT=60

[ -e /etc/sysconfig/$prog ] && . /etc/sysconfig/$prog

lockfile=/var/lock/subsys/$prog


# extract value of a MySQL option from config files
# Usage: get_mysql_option SECTION VARNAME DEFAULT
# result is returned in $result
# We use my_print_defaults which prints all options from multiple files,
# with the more specific ones later; hence take the last match.
get_mysql_option(){
        result=`/usr/bin/my_print_defaults "$1" | sed -n "s/^--$2=//p" | tail -n 1`
        if [ -z "$result" ]; then
            # not found, use default
            result="$3"
        fi
}


get_mysql_option mysqld datadir "/var/lib/mysql"
datadir="$result"
get_mysql_option mysqld socket "$datadir/mysql.sock"
socketfile="$result"
get_mysql_option mysqld_safe log-error "/var/log/mysqld.log"
errlogfile="$result"
get_mysql_option mysqld_safe pid-file "/var/run/mysqld/mysqld.pid"
mypidfile="$result"

start(){
    [ -x $exec ] || exit 5
    # check to see if it's already running
    RESPONSE=`/usr/bin/mysqladmin --socket="$socketfile" --user=UNKNOWN_MYSQL_USER ping 2>&1`
    if [ $? = 0 ]; then
        # already running, do nothing
        action $"Starting $prog: " /bin/true
        ret=0
    elif echo "$RESPONSE" | grep -q "Access denied for user"
    then
        # already running, do nothing
        action $"Starting $prog: " /bin/true
        ret=0
    else
        # prepare for start
        touch "$errlogfile"
        chown mysql:mysql "$errlogfile" 
        chmod 0640 "$errlogfile"
        [ -x /sbin/restorecon ] && /sbin/restorecon "$errlogfile"
        if [ ! -d "$datadir/mysql" ] ; then
            # First, make sure $datadir is there with correct permissions
            if [ ! -e "$datadir" -a ! -h "$datadir" ]
            then
                mkdir -p "$datadir" || exit 1
            fi
            chown mysql:mysql "$datadir"
            chmod 0755 "$datadir"
            [ -x /sbin/restorecon ] && /sbin/restorecon "$datadir"
            # Now create the database
            action $"Initializing MySQL database: " /usr/bin/mysql_install_db --rpm --datadir="$datadir" --user=mysql
            ret=$?
            chown -R mysql:mysql "$datadir"
            if [ $ret -ne 0 ] ; then
                return $ret
            fi
        fi
        chown mysql:mysql "$datadir"
        chmod 0755 "$datadir"
        # Pass all the options determined above, to ensure consistent behavior.
        # In many cases mysqld_safe would arrive at the same conclusions anyway
        # but we need to be sure.  (An exception is that we don't force the
        # log-error setting, since this script doesn't really depend on that,
        # and some users might prefer to configure logging to syslog.)
        # Note: set --basedir to prevent probes that might trigger SELinux
        # alarms, per bug #547485
        $exec   --datadir="$datadir" --socket="$socketfile" \
                --pid-file="$mypidfile" \
                --basedir=/usr --user=mysql
    fi
	return $ret
}
start

