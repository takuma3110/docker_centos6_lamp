#!/bin/bash
echo '' > ~/.ssh/known_hosts 

if [ $# -lt 1 ] ;then
	echo "usage: ./ssh_container.sh container [command]"
elif [ $# -lt 2 ] ;then
	ssh root@$(docker inspect --format '{{ .NetworkSettings.IPAddress }}' $1) -o "StrictHostKeyChecking no" -i ./server/keys/id_rsa
else
	ssh root@$(docker inspect --format '{{ .NetworkSettings.IPAddress }}' $1) -o "StrictHostKeyChecking no" -i ./server/keys/id_rsa $2
fi

#echo $1
